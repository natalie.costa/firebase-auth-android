package com.example.auth_firebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class telaEsqueciSenha extends AppCompatActivity implements View.OnClickListener {
    CardView voltar, redefinirSenha;
    EditText emailEsqueciSenha;

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_esqueci_senha);

        inicializarComponentes();
    }

    public void redefinirSenha(){
        String email = emailEsqueciSenha.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            Toast.makeText(telaEsqueciSenha.this, "Insira o e-mail", Toast.LENGTH_LONG).show();
        } else{
            progressDialog.setMessage("Enviando configurações...");
            progressDialog.show();
            firebaseAuth.sendPasswordResetEmail(email)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            progressDialog.dismiss();
                            if(task.isSuccessful()){
                                Toast.makeText(telaEsqueciSenha.this, "Redefinição de senha enviada para o email", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(telaEsqueciSenha.this, "E-mail não cadastrado", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }

    @Override
    public void onClick(View view){
        if(view == voltar){
            startActivity(new Intent(this, MainActivity.class));
        } if(view == redefinirSenha){
            redefinirSenha();
        }

    }
    private void inicializarComponentes(){
        voltar = (CardView) findViewById(R.id.btn_voltar_login);
        emailEsqueciSenha = (EditText) findViewById(R.id.input_email_esqueci_senha);
        redefinirSenha = (CardView) findViewById(R.id.btn_redefinir_senha);
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        voltar.setOnClickListener(this);
        redefinirSenha.setOnClickListener(this);

    }
}
