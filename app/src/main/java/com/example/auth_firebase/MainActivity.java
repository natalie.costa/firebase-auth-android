package com.example.auth_firebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    CardView btnLogin, btnVoltarLogin;
    TextView textRegister, textForgotPassword;
    EditText userName, password;
    CheckBox remember;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firebaseAuth = FirebaseAuth.getInstance();

        inicialyzeComponents();

        FirebaseUser user = firebaseAuth.getCurrentUser();
        Toast.makeText(MainActivity.this, "Seja bem vindo: ", Toast.LENGTH_SHORT).show();
        if (user != null) {// Verifica se o usuario está logado
            startActivity(new Intent(getApplicationContext(), telaPrincipal.class));
        }

    }

    private void userLogin() {
        String email = userName.getText().toString().trim();
        String passw = password.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            //email is empty
            Toast.makeText(this, "Insira o email", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(passw)) {
            //password is empty
            Toast.makeText(this, "Insira a senha", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Logando no sistema...");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email, passw)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if (task.isSuccessful()) {
                            startActivity(new Intent(getApplicationContext(), telaPrincipal.class));
                        }
                    }
                });
    }


    @Override
    public void onClick(View view) {
        if (view == btnLogin) {
            userLogin();
        }
        if (view == textRegister) {
            startActivity(new Intent(MainActivity.this, telaCadastro.class));
        }
        if (view == textForgotPassword) {
            startActivity(new Intent(MainActivity.this, telaEsqueciSenha.class));
        }
        //if (view == remember) {
        //  if(remember.isChecked()){
        //    FirebaseUser getUser = firebaseAuth.getCurrentUser();
        //  if(getUser == null){
        //    startActivity(new Intent(this, telaPrincipal.class));
        //}
        //}
//}
    }

    private void inicialyzeComponents() {
        btnLogin = (CardView) findViewById(R.id.btn_login);
        btnVoltarLogin = (CardView) findViewById(R.id.btn_voltar_login);
        remember = (CheckBox) findViewById(R.id.btn_remember);
        textRegister = (TextView) findViewById(R.id.text_register);
        textForgotPassword = (TextView) findViewById(R.id.text_forgot_password);
        userName = (EditText) findViewById(R.id.input_user);
        password = (EditText) findViewById(R.id.input_password);
        progressDialog = new ProgressDialog(this);
        textRegister.setOnClickListener(this);
        remember.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        textForgotPassword.setOnClickListener(this);
    }
}
